#include <stdio.h>
#include <stdlib.h>
#include <libpq-fe.h>

void doSQL(PGconn *conn, char *command)
{
  PGresult *result;

  printf("%s\n", command);

  result = PQexec(conn, command);
  printf("status is     : %s\n", PQresStatus(PQresultStatus(result)));
  printf("#rows affected: %s\n", PQcmdTuples(result));
  printf("result message: %s\n", PQresultErrorMessage(result));

  switch(PQresultStatus(result)) {
	  case PGRES_TUPLES_OK:
	    {
	      int n = 0, m = 0;
	      int nrows   = PQntuples(result);
	      int nfields = PQnfields(result);
	      printf("number of rows returned   = %d\n", nrows);
	      printf("number of fields returned = %d\n", nfields);
	      for(m = 0; m < nrows; m++) {
			  for(n = 0; n < nfields; n++)
			  	printf(" %s = %s", PQfname(result, n),PQgetvalue(result,m,n));
		  printf("\n");
		  }
	     }
  }
  PQclear(result);
}

int dropTable(PGconn *conn){
	PGresult *result;	
	result = PQexec(conn, "SELECT * FROM person");

	if(PQresultStatus(result) == PGRES_TUPLES_OK){
		doSQL(conn, "DROP TABLE person");	
	}
}

int createTable(PGconn *conn){

	dropTable(conn);
    doSQL(conn, "CREATE TABLE person(id serial primary key, LastName varchar(255), FirstName varchar(255), Street varchar(255), HouseNumber INTEGER, City varchar(255), TotalIncome MONEY DEFAULT 1000, calcedTaxYear MONEY, DateCreation DATE DEFAULT now() )");
}

int printTable(PGconn *conn){
	PGresult *result;	
	result = PQexec(conn, "SELECT * FROM person");

	if(PQresultStatus(result) != PGRES_TUPLES_OK){
		printf("\nNIE MA TABELI person\n");
		return -1;
	}

	doSQL(conn, "SELECT * FROM person");	
}

int createTrigger(PGconn *conn){
	PGresult *result;	
	result = PQexec(conn, "SELECT * FROM person");

	if(PQresultStatus(result) != PGRES_TUPLES_OK){
		printf("\nNIE MA TABELI person\n");
		return -1;
	}
	dropTrigger(conn);

	doSQL(conn, "CREATE OR REPLACE FUNCTION calcTax() RETURNS TRIGGER AS $$ BEGIN NEW.FirstName := upper(substring(NEW.FirstName from 1 for 1)) || substring(NEW.FirstName from 2 for length(NEW.FirstName)); NEW.LastName := upper(substring(NEW.LastName from 1 for 1)) || substring(NEW.LastName from 2 for length(NEW.LastName)); NEW.Street := upper(substring(NEW.Street from 1 for 1)) || substring(NEW.Street from 2 for length(NEW.Street)); NEW.City := upper(substring(NEW.City from 1 for 1)) || substring(NEW.City from 2 for length(NEW.City)); NEW.calcedTaxYear := NEW.TotalIncome * 12 * 0.2; RETURN NEW; END; $$ LANGUAGE 'plpgsql';");
    doSQL(conn, "CREATE TRIGGER person_insert BEFORE INSERT ON person FOR EACH ROW EXECUTE PROCEDURE calcTax();");
    doSQL(conn, "CREATE TRIGGER person_update BEFORE UPDATE ON person FOR EACH ROW EXECUTE PROCEDURE calcTax();");
}

int dropTrigger(PGconn *conn){
	PQexec(conn, "DROP TRIGGER person_insert;");
	PQexec(conn, "DROP TRIGGER person_update;");
}

int search(PGconn *conn){
	char search[255];
	char zapytanie[500];
	char dump;
	int choice=0;
	PGresult *result;	
	result = PQexec(conn, "SELECT * FROM person");

	if(PQresultStatus(result) != PGRES_TUPLES_OK){
		printf("\nNIE MA TABELI person\n");
		return -1;
	}

	do{
		printf("------------------------\n");
		printf("Wybierz spsosob szukania\n\n");
		printf("1 - Nazwisko\n");
		printf("2 - Imie\n");
		printf("------------------------\n");
		scanf("%d",&choice);
	}
	while(choice<1 || choice>2);

	switch(choice){
		case 1:
			printf("\n Nazwisko do szuaknia:\t");
			scanf ("%s", search);
			scanf("%c", &dump);
			sprintf(zapytanie, "SELECT * FROM person WHERE LastName=\'%s\'",search);	
			break;
		case 2:
			printf("\n Imie do szuaknia:\t");
			scanf ("%s", search);
			scanf("%c", &dump);
			sprintf(zapytanie, "SELECT * FROM person WHERE FirstName=\'%s\'",search);	
			break;
	}
	
	doSQL(conn, zapytanie);	
}


int delete(PGconn *conn){
	char LastName[255];
	char zapytanie[500];
	char dump;
	PGresult *result;	
	result = PQexec(conn, "SELECT * FROM person");

	if(PQresultStatus(result) != PGRES_TUPLES_OK){
		printf("\nNIE MA TABELI person\n");
		return -1;
	}

	printf("\n Nazwisko osoby do usuniecia:\t");
	scanf ("%s", LastName);
	scanf("%c", &dump);
	
	sprintf(zapytanie, "DELETE FROM person WHERE LastName=\'%s\'",LastName);
	doSQL(conn, zapytanie);
}

int updatePerson(PGconn *conn){
	char LastNameOld[255], LastName[255];
	char FirstNameOld[255], FirstName[255];
	char StreetOld[255], Street[255];
	int HouseNumberOld, HouseNumber;
	char CityOld[255], City[255];
	int TotalIncome;
	char dump;
	char zapytanie[500];


	int ile;
	int wybor;

	PGresult *result;	
	result = PQexec(conn, "SELECT * FROM person");

	if(PQresultStatus(result) != PGRES_TUPLES_OK){
		printf("\nNIE MA TABELI person\n");
		return -1;
	}

	printf("\nIle osob chcesz edytowac?\n");
	scanf("%d",&ile);

	while(ile>0){
		ile--;

		do{
			printf("Jakie dane chcesz edytowac?\n");
			printf("1 - Nazwisko\n");
			printf("2 - Imie\n");
			printf("3 - Ulica\n");
			printf("4 - Numerdomu\n");
			printf("5 - Miasto\n");
			scanf("%d", &wybor);
		}while(wybor<1 || wybor>6);

		switch(wybor){
			case 1:
					printf("Podaj Stare Nazwisko:\t");
					scanf ("%s", LastNameOld);
					scanf("%c", &dump);
					printf("Podaj Nowe Nazwisko:\t");
					scanf ("%s", LastName);
					scanf("%c", &dump);
					sprintf(zapytanie, " UPDATE person SET LastName = \'%s\' WHERE LastName = \'%s\'", LastName,LastNameOld);
					break;	
			case 2:
					printf("Podaj Stare Imie:\t");
					scanf ("%s", FirstNameOld);
					scanf("%c", &dump);
					printf("Podaj Nowe Imie:\t");
					scanf ("%s", FirstName);
					scanf("%c", &dump);
					sprintf(zapytanie, " UPDATE person SET FirstName = \'%s\' WHERE FirstName = \'%s\'", FirstName,FirstNameOld);
					break;
			case 3:
					printf("Podaj Stare Ulice:\t");
					scanf ("%s", StreetOld);
					scanf("%c", &dump);
					printf("Podaj Nowe Ulice:\t");
					scanf ("%s", Street);
					scanf("%c", &dump);
					sprintf(zapytanie, " UPDATE person SET Street = \'%s\' WHERE Street = \'%s\'", Street,StreetOld);
					break;	
 			case 4:
					printf("Podaj Stary Numer domu:\t");
					scanf ("%d", &HouseNumberOld);
					printf("Podaj Nowy Numer domu:\t");
					scanf ("%d", &HouseNumber);
					sprintf(zapytanie, " UPDATE person SET HouseNumber = %d WHERE HouseNumber = %d", HouseNumber,HouseNumberOld);
					break;
			case 5:
					printf("Podaj Stare Miasto:\t");
					scanf ("%s", CityOld);
					scanf("%c", &dump);
					printf("Podaj Nowe Miasto:\t");
					scanf ("%s", City);
					scanf("%c", &dump);
					sprintf(zapytanie, " UPDATE person SET City = \'%s\' WHERE City = \'%s\'", City,CityOld);
					break;														
		}

		doSQL(conn, zapytanie);	
	}

}


int insertValues(PGconn *conn){
	
	PGresult *result;	
	result = PQexec(conn, "SELECT * FROM person");

	if(PQresultStatus(result) == PGRES_TUPLES_OK){

	char LastName[255];
	char FirstName[255];
	char Street[255];
	int HouseNumber;
	char City[255];
	int TotalIncome;
	char dump;
	char zapytanie[500];	

	printf("Podaj Nazwisko:\t");
	scanf ("%s", LastName);
	scanf("%c", &dump);
	
	printf("\nPodaj Imie:\t");
	scanf ("%s", FirstName);
	scanf("%c", &dump);

	printf("\nPodaj Ulica:\t");
	scanf ("%s", Street);
	scanf("%c", &dump);

	printf("\nPodaj Numer domu:\t");
	scanf ("%d", &HouseNumber);
	
	printf("\nPodaj Miasto:\t");
	scanf ("%s", City);
	scanf("%c", &dump);				

	printf("\nPodaj Pensje:\t");
	scanf ("%d", &TotalIncome);
	
	sprintf(zapytanie, "INSERT INTO person(LastName, FirstName, Street, HouseNumber, City, TotalIncome) VALUES(\'%s\', \'%s\', \'%s\', %d , \'%s\', %d)",LastName, FirstName, Street, HouseNumber, City, TotalIncome);

	printf("%s\n", zapytanie);

	doSQL(conn, zapytanie);	
	}
	else{
		printf("\nNIE MA TABELI person\n");
	}
}

int menu(){
	int selection = -1;
	 
	do
	{
		if((selection != -1)  && ((selection < 0) || (selection > 9))){
			printf("Wprowadziles zle dane\n");
		}

	printf("\n----------------------------\n");
	printf("|\t  Menu\t\t   |\n");
	printf("----------------------------\n");

    printf("1 - Stworz Tabele\n");
	printf("2 - Usun Tabele\n");
	printf("3 - Stworz Wyzwalacz\n");
	printf("4 - Usun Wyzwalacz\n");
	printf("5 - Dodaj Rekord do tabeli\n");
	printf("6 - Usun rekord z tabeli\n");
	printf("7 - Edytuj rekord w tabeli\n");
	printf("8 - Wyswietl zawartosc tabeli\n");
	printf("9 - Wyszukaj\n");
	printf("0 - Koniec\n");
	
	printf("\nWybierz opcje: ");
	scanf("%d", &selection );
	
	} while ((selection < 0) || (selection > 9));
	
	return selection;
}


int main()
{
  PGresult *result;
  PGconn   *conn;
  int x;

  conn = PQconnectdb("host=localhost port=5432 dbname= user= password=");

  if(PQstatus(conn) == CONNECTION_OK) {
    printf("connection made\n");
 
    while((x = menu()) != 0){
    	printf("\n\t%d\n",x);
    	switch(x){
    		case 1:
    			createTable(conn);
    			break;
    		case 2:
    			dropTable(conn);
    			break;
    		case 3:
    			createTrigger(conn);
    			break;
    		case 4:
    			dropTrigger(conn);
    			break;
    		case 5:
    			insertValues(conn);
    			break;
    		case 6:
    			delete(conn);
    			break;
    		case 7:
    			updatePerson(conn);
    			break;
    		case 8:
    			printTable(conn);
    			break;
    		case 9:
    			search(conn);
    			break;
    	}
    } 

  }
  else
    printf("connection failed: %s\n", PQerrorMessage(conn));

  PQfinish(conn);
  return EXIT_SUCCESS;
}